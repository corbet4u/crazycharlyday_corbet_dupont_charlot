<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;


class ConnexionView
{

    private $erreur;

    public function __construct($e = false)
    {
        $this->erreur = $e;
    }

    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor('route_home', []);
        $url2 = $app->urlFor('connect', []);
        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body>
</div>
<form method="post" action="$url2">
<div class="row">
    <form class="col s12">
    <div class="row">
          <div class="input-field col s12">
          <input required name='login' id="login" type="text" class="validate">
          <label for="login">nom d'utilisateur</label>
        </div>
      </div>
      
      <div class="row">
        <div class="input-field col s12">
          <input required name='password' id="password" type="password" class="validate">
          <label for="password">Mot de passe</label>
        </div>
      </div>
      <div class="row">
        <button class="col offset-l4 s4 m4 l4 btn waves-effect waves-light" type="submit"> Valider </button>
      </div>
    </form>
  </div>

</form>
END;
        if(!$this->erreur)
            $html = $html .'</body><html>';
        else
            $html = $html . '<p>Mot de passe ou nom d utilisateur incorrect</p></body><html>';


        return Connect::getHeader() . $html;
    }

}
