<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;


class ModifPassView
{
    public function render()
    {
        $app = \Slim\Slim::getInstance();

        $url = $app->urlFor('modifmdp', []);

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body>
<div class="row">
    <form method="post" class="col sl2" action="$url">
      <div class="row">
        <div class="input-field col s12">
          <input name='password' id="password" type="password" class="validate">
          <label for="password">Ancien mot de passe</label>
        </div>
        <div class="input-field col s12">
          <input required name='npassword' id="npassword" type="password" class="validate">
          <label for="npassword">Nouveau mot de passe</label>
        </div>
</div>
</div>
 <div class="row">
<input type="submit" value="Valider" />
</div>
</form>
</div>
</body><html> 
END;

        return Connect::getHeader() . $html;
    }
}