<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 09/01/2018
 * Time: 19:16
 */

namespace mywishlist\View;

use mywishlist\Controller\Connect;
use mywishlist\Models\User;

class ModifInfoView
{
    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $url2 = $app->urlFor('modifinfo');
        $user = User:: where('id', '=', $_SESSION['id'])->first();
        $nom = $user->nom;
        $prenom = $user->prenom;
        $mail = $user->email;

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();

        $html = $html . <<<END
<body>

   <form method="post" action="$url2">
<div class="row">
    <form class="col s12">
   
      <div class="row">
        <div class="input-field col s6">
          <input required name='nom' value="$nom" placeholder="$nom" id="first_name" type="text" class="validate">
          <label for="first_name">Nom</label>
        </div>
        <div class="input-field col s6">
          <input required name='prenom' value="$prenom" placeholder="$prenom" id="last_name" type="text" class="validate">
          <label for="last_name">Prenom</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input required name='email' value="$mail" placeholder="$mail" id="email" type="email" class="validate">
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row">
        <input class="waves-effect waves-light btn" type="submit" value="Valider" />
      </div>
    </form>
  </div>

</form>
</body><html> 
END;

        return Connect::getHeader() . $html;
    }
}