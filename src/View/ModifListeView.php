<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;
use mywishlist\Models\Liste;

class ModifListeView
{
    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $url2 = $app->urlFor('modifListe');
        $l = Liste:: where('no', '=', $_SESSION['liste'])->first();
        $titre = $l->titre;
        $descr = $l->description;

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();

        $html = $html . <<<END
<body>

   <form method="post" action="$url2">
<div class="row">
    <form class="col s12">
   
      <div class="row">
        <div class="input-field col s6">
          <input required name='titre' value="$titre" placeholder="$titre" id="first_name" type="text" class="validate">
          <label for="titre">Titre</label>
        </div>
        <div class="input-field col s6">
          <input name='description' value="$descr" placeholder="$descr" id="last_name" type="text" class="validate">
          <label for="description">Description</label>
        </div>
      </div>
      <div class="row">
        <input class="waves-effect waves-light btn" type="submit" value="Valider" />
      </div>
    </form>
  </div>

</form>
</body><html> 
END;

        return Connect::getHeader() . $html;
    }
}