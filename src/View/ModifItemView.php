<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;
use mywishlist\Models\Item;

class ModifItemView
{
    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $url2 = $app->urlFor('modifItem', ['id'=>$_SESSION['item']]);
        $l = Item:: where('id', '=', $_SESSION['item'])->first();
        $titre = $l->nom;
        $descr = $l->descr;
        $tarif = $l->tarif;
        $image = $l->img;
        $vente = $l->url;

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();

        $html = $html . <<<END
<body>

   <form method="post" action="$url2">
<div class="row">
    <form class="col s12">
   
      <div class="row">
        <div class="input-field col s6">
          <input required name='titre' value="$titre" placeholder="$titre" id="titre" type="text" class="validate">
          <label for="titre">Titre</label>
        </div>
        <div class="input-field col s6">
          <input name='description' value="$descr" placeholder="$descr" id="description" type="text" class="validate">
          <label for="description">Description</label>
        </div>
        <div class="input-field col s6">
          <input required name='tarif' value="$tarif" placeholder="$tarif" id="tarif" type="text" class="validate">
          <label for="tarif">Tarif</label>
        </div>
        <div class="input-field col s6">
          <input name='image' value="$image" placeholder="$image" id="image" type="text" class="validate">
          <label for="image">Lien Image</label>
        </div>
        <div class="input-field col s6">
          <input name='vente' value="$vente" placeholder="$vente" id="vente" type="text" class="validate">
          <label for="vente">Lien commercial</label>
        </div>
      </div>
      <div class="row">
        <input class="waves-effect waves-light btn" type="submit" value="Valider" />
      </div>
    </form>
  </div>

</form>
</body><html> 
END;

        return Connect::getHeader() . $html;
    }
}