<?php
namespace ccd\Models ;

    class Item extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'item';
        protected $primaryKey = 'id';
        public $timestamps = false;

        function getTest($id) {
            $items = Item::get();
            $arr = null;
            foreach ($items as $item){
                if($item->liste_id == $id)
                    $arr[] = $item;
            }
            return $arr;
        }

        function getListe(){
            return $this->liste_id;
        }

        function getItem($id) {
            $items = Item::get();
            foreach ($items as $item){
                if($item->id == $id)
                    $arr = $item;
            }
            return $arr;
        }

        public static function createItem($n,$d,$t,$i,$v,$c){
            $l = new Item();
            $l->nom = $n;
            $l->descr = $d;
            $l->tarif = $t;
            $l->img = $i;
            $l->url = $v;
            if($c == 'on')
                $l->cagnotte = $c;
            else
                $l->cagnotte = 'off';
            $l->liste_id = $_SESSION['liste'];

            $l->save();
        }

        public function liste(){
    	    return $this->belongsTo('mywishlist\models\Liste','liste_id') ;
        }
}
