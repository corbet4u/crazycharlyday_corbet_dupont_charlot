<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 08/02/2018
 * Time: 10:14
 */

namespace ccd\Models;


class Categorie extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;
}