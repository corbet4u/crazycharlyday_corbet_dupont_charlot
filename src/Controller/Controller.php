<?php

namespace ccd\Controller;

use ccd\Models\Liste;
use ccd\View\CagnotteView;
use ccd\View\ConnexionView;
use ccd\View\CreatorView;
use ccd\View\HomeView;
use ccd\View\InscriptionViewer;
use ccd\View\ItemView;
use ccd\View\ItemListView;
use ccd\View\listepublicView;
use ccd\View\ListView;
use ccd\View\ModifInfoView;
use ccd\View\ModifItemView;
use ccd\View\ModifPassView;
use ccd\View\MoncompteView;
use ccd\View\NewListView;
use ccd\View\NewItemView;
use ccd\View\ReservationItem;
use ccd\View\SuppressionView;
use ccd\View\ModifListeView;


class Controller
{
    static function showConnection()
    {
        $v = new ConnexionView();
        echo $v->render();
    }

    static function showHome()
    {
        $v = new HomeView();
        echo $v->render();
    }

    static function showInscription()
    {
        $v = new InscriptionViewer();
        echo $v->render();
    }

    static function showItem($id)
    {
        $_SESSION['item'] = $id;
        $v = new ItemView($id);
        echo $v->render();
    }

    static function showItemList($num)
    {
        $_SESSION['liste'] = Liste::getNo($num);
        $v = new ItemListView($num);
        echo $v->render();
    }

    static function showList()
    {
        $v = new ListView();
        echo $v->render();
    }

    static function showModifInfo()
    {
        $v = new ModifInfoView();
        echo $v->render();
    }

    static function showModifPass()
    {
        $v = new ModifPassView();
        echo $v->render();
    }

    static function showMonCompte()
    {
        $v = new MoncompteView();
        echo $v->render();
    }

    static function showNewItem()
    {
        $v = new NewItemView();
        echo $v->render();
    }

    static function showNewList()
    {
        $v = new NewListView();
        echo $v->render();
    }

    static function showSuppression(){
        $v = new SuppressionView();
        echo $v->render();
    }

    static function showModifListe(){
        $v = new ModifListeView();
        echo $v->render();
    }

    static function showModifItem(){
        $v = new ModifItemView();
        echo $v->render();
    }

    static function showReservItem($id){
        $v = new ReservationItem();
        echo $v->render($id);
    }

    static function showListepublic(){
        $v = new listepublicView();
        echo $v->render();
    }

    static function showCagnotte($id){
        $v = new CagnotteView($id);
        echo $v->render();
    }

    static function showCreator(){
        $v = new CreatorView();
        echo $v->render();
    }
}